# Spatiotemporal Directional Number Graph

## Description

This code presents a local spatiotemporal encoding mechanism that transform a volumetric data (video sequence) into a sequence descriptor (constructed by graphs, or adjancency matrices).

## Usage

To code a given image you can use this code by

```matlab
% default invocation
C = dng('dir/to/faces');

% using threshold 15, mask Sobel, and neighborhood size of 5x5)
C = dng('dir/to/faces','mask', '3d');
```

## Citation

If you use this code please cite

> Ramírez Rivera, A.; Chae, O., "Spatiotemporal Directional Number Transitional Graph for Dynamic Texture Recognition," Pattern Analysis and Machine Intelligence, IEEE Transactions on , vol.PP, no.99, p.1,1 
> doi: 10.1109/TPAMI.2015.2392774

```tex
@Article{RamirezRivera2015,
  Title                    = {Spatiotemporal Directional Number Transitional Graph for Dynamic Texture Recognition},
  Author                   = {Ram\'irez Rivera, A. and Chae, O.},
  Journal                  = {IEEE Transactions on Pattern Analysis and Machine Intelligence},
  Year                     = {2015},
  Number                   = {10},
  Pages                    = {2146--2152},
  Volume                   = {37},
  Doi                      = {10.1109/TPAMI.2015.2392774},
  ISSN                     = {0162-8828}
}
```