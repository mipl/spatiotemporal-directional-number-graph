function [H] = dng(faceDir,varargin)
% Spatiotemporal Directional Number Graph
% 
% If you use this code please cite
% Ramírez Rivera, A.; Chae, O., "Spatiotemporal Directional Number
% Transitional Graph for Dynamic Texture Recognition," Pattern Analysis and Machine Intelligence, IEEE Transactions on , vol.PP, no.99, pp.1,1 
% doi: 10.1109/TPAMI.2015.2392774
%
% Parameters:
% faceDir: the directory where the face images are (of a single person)
% Output
% H: adjacency matrix of the generated graphs
%
% Optional parameters, inputed as string, value pairs: 
% grid:        the grid size in which the image sequence is split (default: 
%              [3 3 3])
% resize:      the size of the image (default: [110 100])
% normalized:  whether to normalize the values of the adjacency matrices
%              (default: true)
% overlapping: percentage of overlap in the grid. The grid dimensions are 
%              used to compute the size of the cell to be used, and then the 
%              grid is recomputed with the overlap in place (default: 0)
% masks:       the mask to compute the edge responses (supported: kirsch, 3d, 
%              3d-kirsch, or 3d-sobel) (default: kirsch)
% data:        instead of reading the images of the faceDir, a cube with
%              the data can be passed in this parameter (default: [])
% rank:        the rank order of the final code (default: 2)


%% Variables
% image parameters
r = 110;
c = 100;
grid = [3 3 3];

% method parameters
topRank = 2;
masks = 'kirsch'; %3d

% others
normilized = true; % whether to normilize
overlapping = 0;

data = [];

%% parse vars
optargin = size(varargin, 2);

i = 1;
while (i <= optargin)
  if (strcmp(varargin{i}, 'grid'))
    if (i >= optargin)
      error('argument required for %s', varargin{i});
    else
      grid = varargin{i+1};
      i = i + 2;
    end
  elseif (strcmp(varargin{i}, 'resize'))
    if (i >= optargin)
      error('argument required for %s', varargin{i});
    else
      sz = varargin{i+1};
      r = sz(1);
      c = sz(2);
      i = i + 2;
    end
  elseif (strcmp(varargin{i}, 'normalized'))
    if (i >= optargin)
      error('argument required for %s', varargin{i});
    else
      normilized = varargin{i+1};
      i = i + 2;
    end
  elseif (strcmp(varargin{i}, 'overlapping'))
    if (i >= optargin)
      error('argument required for %s', varargin{i});
    else
      overlapping = varargin{i+1};
      i = i + 2;
    end
  elseif (strcmp(varargin{i}, 'masks'))
    if (i >= optargin)
      error('argument required for %s', varargin{i});
    else
      masks = varargin{i+1};
      i = i + 2;
    end
  elseif (strcmp(varargin{i}, 'data'))
    if (i >= optargin)
      error('argument required for %s', varargin{i});
    else
      data = varargin{i+1};
      i = i + 2;
    end
  elseif (strcmp(varargin{i}, 'rank'))
    if (i >= optargin)
      error('argument required for %s', varargin{i});
    else
      topRank = varargin{i+1};
      i = i + 2;
    end
  else
    error('argument %s is does not exist',varargin{i});
  end
end

%% Directory checking
if (isempty(data) && ~exist(faceDir,'dir'))
  error('The directory %s does not exist. Check the path.',faceDir);
elseif isempty(data)
  % ensure that dir path ends with backslash
  if strcmp(faceDir(end),filesep) == 0
    faceDir = strcat(faceDir,filesep);
  end
end

%% Masks
switch lower(masks)
  case 'kirsch'
    % Kirsch masks
    D(:,:,1) = [-3 -3  5; -3  0  5; -3 -3  5];      %East
    D(:,:,2) = [-3  5, 5; -3  0  5; -3 -3 -3];      %North East
    D(:,:,3) = [ 5  5  5; -3  0 -3; -3 -3 -3];      %North
    D(:,:,4) = [ 5  5 -3;  5  0 -3; -3 -3 -3];      %North West
    D(:,:,5) = [ 5 -3 -3;  5  0 -3;  5 -3 -3];      %West
    D(:,:,6) = [-3 -3 -3;  5  0 -3;  5  5 -3];      %South West
    D(:,:,7) = [-3 -3 -3; -3  0 -3;  5  5  5];      %South
    D(:,:,8) = [-3 -3 -3; -3  0  5; -3  5  5];      %South Eest
    % code length
    lgth = 8;
    %always use top rank 2 for kirsch
    % warning('Using topRank=2');
    % topRank = 2;
    
  case '3d'
    % My 3D sobel masks
    %XT plane
    D(:,:,1,1) = [ 1  0 -1; 2  0 -2; 1  0 -1];
    D(:,:,2,1) = [ 2  0 -2; 4  0 -4; 2  0 -2];
    D(:,:,3,1) = [ 1  0 -1; 2  0 -2; 1  0 -1];
    %XT 45X
    D(:,:,1,2) = [ 2  1  0; 4  2  0; 2  1  0];
    D(:,:,2,2) = [ 1  0 -1; 2  0 -2; 1  0 -1];
    D(:,:,3,2) = [ 0 -1 -2; 0 -2 -4; 0 -1 -2];
    %XT 90X
    D(:,:,1,3) = [ 1  2  1; 2  4  2; 1  2  1];
    D(:,:,2,3) = [ 0  0  0; 0  0  0; 0  0  0];
    D(:,:,3,3) = [-1 -2 -1;-2 -4 -2;-1 -2 -1];
    %XT 135X
    D(:,:,1,4) = [ 0  1  2; 0  2  4; 0  1  2];
    D(:,:,2,4) = [-1  0  1;-2  0  2;-1  0  1];
    D(:,:,3,4) = [-2 -1  0;-4 -2  0;-2 -1  0];
    %YT = XT 90T
    D(:,:,1,5) = [ 1  2  1; 0  0  0;-1 -2 -1];
    D(:,:,2,5) = [ 2  4  2; 0  0  0;-2 -4 -2];
    D(:,:,3,5) = [ 1  2  1; 0  0  0;-1 -2 -1];
    %YT 45T
    D(:,:,1,6) = [ 0  1  2;-1  0  1;-2 -1  0];
    D(:,:,2,6) = [ 0  2  4;-2  0  2;-4 -2  0];
    D(:,:,3,6) = [ 0  1  2;-1  0  1;-2 -1  0];
    %YT 90T = XT
    %YT 135T
    D(:,:,1,7) = [ 2  1  0; 1  0 -1; 0 -1 -2];
    D(:,:,2,7) = [ 4  2  0; 2  0 -2; 0 -2 -4];
    D(:,:,3,7) = [ 2  1  0; 1  0 -1; 0 -1 -2];
    %YT 45Y
    D(:,:,1,8) = [ 0  0  0;-1 -2 -1;-2 -4 -2];
    D(:,:,2,8) = [ 1  2  1; 0  0  0;-1 -2 -1];
    D(:,:,3,8) = [ 2  4  2; 1  2  1; 0  0  0];
    %YT 90Y = XT 90X
    %YT 135Y
    D(:,:,1,9) = [ 2  4  2; 1  2  1; 0  0  0];
    D(:,:,2,9) = [ 1  2  1; 0  0  0;-1 -2 -1];
    D(:,:,3,9) = [ 0  0  0;-1 -2 -1;-2 -4 -2];

    % Out of cube rotation (corner direction)
    % these shifts are not used in the method, but are here for completeness
    D(:,:,1,10) = [ 4  2  1; 2  1  0; 0  0 -1];
    D(:,:,2,10) = [ 2  1  0; 1  0 -1; 0 -1 -2];
    D(:,:,3,10) = [ 1  0  0; 0 -1 -2;-1 -2 -4];
    
    D(:,:,1,11) = [ 0  0  1;-2 -1  0;-4 -2 -1];
    D(:,:,2,11) = [ 0  1  2;-1  0  1;-2 -1  0];
    D(:,:,3,11) = [ 1  2  4; 0  1  2;-1  0  0];
    
    D(:,:,1,12) = [ 1  2  4; 0  1  2;-1  0  0];
    D(:,:,2,12) = [ 0  1  2;-1  0  1;-2 -1  0];
    D(:,:,3,12) = [ 0  0  1;-2 -1  0;-4 -2 -1];
    
    D(:,:,1,13) = [ 1  0  0; 0 -1 -2;-1 -2 -4];
    D(:,:,2,13) = [ 2  1  0; 1  0 -1; 0 -1 -2];
    D(:,:,3,13) = [ 4  2  1; 2  1  0; 0  0 -1];
    
    % code length
    lgth = 9;
    
  case '3d-sobel'
    % 3D sobel masks from "The Edge Point Detection Problem in Image Sequences:
    % Definition and Comparative Evaluation of Some 3D Edge Detecting Schemes"
    %XT plane
    D(:,:,1,1) = [ 1  0 -1; 1  0 -1; 1  0 -1];
    D(:,:,2,1) = [ 1  0 -1; 2  0 -2; 1  0 -1];
    D(:,:,3,1) = [ 1  0 -1; 1  0 -1; 1  0 -1];
    %XT 45X
    D(:,:,1,2) = [ 1  1  0; 2  1  0; 1  1  0];
    D(:,:,2,2) = [ 1  0 -1; 1  0 -1; 1  0 -1];
    D(:,:,3,2) = [ 0 -1 -1; 0 -1 -2; 0 -1 -1];
    %XT 90X
    D(:,:,1,3) = [ 1  1  1; 1  2  1; 1  1  1];
    D(:,:,2,3) = [ 0  0  0; 0  0  0; 0  0  0];
    D(:,:,3,3) = [-1 -1 -1;-1 -2 -1;-1 -1 -1];
    %XT 135X
    D(:,:,1,4) = [ 0  1  1; 0  1  2; 0  1  1];
    D(:,:,2,4) = [-1  0  1;-1  0  1;-1  0  1];
    D(:,:,3,4) = [-1 -1  0;-2 -1  0;-1 -1  0];
    %YT = XT 90T
    D(:,:,1,5) = [ 1  1  1; 0  0  0;-1 -1 -1];
    D(:,:,2,5) = [ 1  2  1; 0  0  0;-1 -2 -1];
    D(:,:,3,5) = [ 1  1  1; 0  0  0;-1 -1 -1];
    %YT 45T
    D(:,:,1,6) = [ 0  1  1;-1  0  1;-1 -1  0];
    D(:,:,2,6) = [ 0  1  2;-1  0  1;-2 -1  0];
    D(:,:,3,6) = [ 0  1  1;-1  0  1;-1 -1  0];
    %YT 90T = XT
    %YT 135T
    D(:,:,1,7) = [ 1  1  0; 1  0 -1; 0 -1 -1];
    D(:,:,2,7) = [ 2  1  0; 1  0 -1; 0 -1 -2];
    D(:,:,3,7) = [ 1  1  0; 1  0 -1; 0 -1 -1];
    %YT 45Y
    D(:,:,1,8) = [ 0  0  0;-1 -1 -1;-1 -2 -1];
    D(:,:,2,8) = [ 1  1  1; 0  0  0;-1 -1 -1];
    D(:,:,3,8) = [ 1  2  1; 1  1  1; 0  0  0];
    %YT 90Y = XT 90X
    %YT 135Y
    D(:,:,1,9) = [ 1  2  1; 1  1  1; 0  0  0];
    D(:,:,2,9) = [ 1  1  1; 0  0  0;-1 -1 -1];
    D(:,:,3,9) = [ 0  0  0;-1 -1 -1;-1 -2 -1];
    % code length
    lgth = 9;
  case '3d-kirsch'
    % 3D sobel masks from "The Edge Point Detection Problem in Image Sequences:
    % Definition and Comparative Evaluation of Some 3D Edge Detecting Schemes"
    %XT plane
    D(:,:,1,1) = [ 17 -9 -9; 17 -9 -9; 17 -9 -9];
    D(:,:,2,1) = [ 17 -9 -9; 17  0 -9; 17 -9 -9];
    D(:,:,3,1) = [ 17 -9 -9; 17 -9 -9; 17 -9 -9];
    %
    D(:,:,1,2) = [ -9 -9 17; -9 -9 17; -9 -9 17];
    D(:,:,2,2) = [ -9 -9 17; -9  0 17; -9 -9 17];
    D(:,:,3,2) = [ -9 -9 17; -9 -9 17; -9 -9 17];
    %XT 45X
    D(:,:,1,3) = [ 17 17 -9; 17 17 -9; 17 17 -9];
    D(:,:,2,3) = [ 17 -9 -9; 17  0 -9; 17 -9 -9];
    D(:,:,3,3) = [ -9 -9 -9; -9 -9 -9; -9 -9 -9];
    %
    D(:,:,1,4) = [ -9 -9 -9; -9 -9 -9; -9 -9 -9];
    D(:,:,2,4) = [ -9 -9 17; -9  0 17; -9 -9 17];
    D(:,:,3,4) = [ -9 17 17; -9 17 17; -9 17 17];
    %XT 90X
    D(:,:,1,5) = [ 17 17 17; 17 17 17; 17 17 17];
    D(:,:,2,5) = [ -9 -9 -9; -9  0 -9; -9 -9 -9];
    D(:,:,3,5) = [ -9 -9 -9; -9 -9 -9; -9 -9 -9];
    %
    D(:,:,1,6) = [ -9 -9 -9; -9 -9 -9; -9 -9 -9];
    D(:,:,2,6) = [ -9 -9 -9; -9  0 -9; -9 -9 -9];
    D(:,:,3,6) = [ 17 17 17; 17 17 17; 17 17 17];
    %XT 135X
    D(:,:,1,7) = [ -9 17 17; -9 17 17; -9 17 17];
    D(:,:,2,7) = [ -9 -9 17; -9  0 17; -9 -9 17];
    D(:,:,3,7) = [ -9 -9 -9; -9 -9 -9; -9 -9 -9];
    %
    D(:,:,1,8) = [ -9 -9 -9; -9 -9 -9; -9 -9 -9];
    D(:,:,2,8) = [ 17 -9 -9; 17  0 -9; 17 -9 -9];
    D(:,:,3,8) = [ 17 17 -9; 17 17 -9; 17 17 -9];
    %YT = XT 90T
    D(:,:,1,9) = [ 17 17 17; -9 -9 -9; -9 -9 -9];
    D(:,:,2,9) = [ 17 17 17; -9  0 -9; -9 -9 -9];
    D(:,:,3,9) = [ 17 17 17; -9 -9 -9; -9 -9 -9];
    %
    D(:,:,1,10) = [ -9 -9 -9; -9 -9 -9; 17 17 17];
    D(:,:,2,10) = [ -9 -9 -9; -9  0 -9; 17 17 17];
    D(:,:,3,10) = [ -9 -9 -9; -9 -9 -9; 17 17 17];
    %YT 45T
    D(:,:,1,11) = [ -9 17 17; -9 -9 17; -9 -9 -9];
    D(:,:,2,11) = [ -9 17 17; -9  0 17; -9 -9 -9];
    D(:,:,3,11) = [ -9 17 17; -9 -9 17; -9 -9 -9];
    %
    D(:,:,1,12) = [ -9 -9 -9; -9 -9 17; -9 17 17];
    D(:,:,2,12) = [ -9 -9 -9; -9  0 17; -9 17 17];
    D(:,:,3,12) = [ -9 -9 -9; -9 -9 17; -9 17 17];
    %YT 90T = XT
    %YT 135T
    D(:,:,1,13) = [ 17 17 -9; 17 -9 -9; -9 -9 -9];
    D(:,:,2,13) = [ 17 17 -9; 17  0 -9; -9 -9 -9];
    D(:,:,3,13) = [ 17 17 -9; 17 -9 -9; -9 -9 -9];
    %
    D(:,:,1,14) = [ -9 -9 -9; 17 -9 -9; 17 17 -9];
    D(:,:,2,14) = [ -9 -9 -9; 17  0 -9; 17 17 -9];
    D(:,:,3,14) = [ -9 -9 -9; 17 -9 -9; 17 17 -9];
    %YT 45Y
    D(:,:,1,15) = [ -9 -9 -9; -9 -9 -9; -9 -9 -9];
    D(:,:,2,15) = [ 17 17 17; -9  0 -9; -9 -9 -9];
    D(:,:,3,15) = [ 17 17 17; 17 17 17; -9 -9 -9];
    %
    D(:,:,1,16) = [ -9 -9 -9; -9 -9 -9; -9 -9 -9];
    D(:,:,2,16) = [ -9 -9 -9; -9  0 -9; 17 17 17];
    D(:,:,3,16) = [ -9 -9 -9; 17 17 17; 17 17 17];
    %YT 90Y = XT 90X
    %YT 135Y
    D(:,:,1,17) = [ 17 17 17; 17 17 17; -9 -9 -9];
    D(:,:,2,17) = [ 17 17 17; -9  0 -9; -9 -9 -9];
    D(:,:,3,17) = [ -9 -9 -9; -9 -9 -9; -9 -9 -9];
    %
    D(:,:,1,18) = [ -9 -9 -9; 17 17 17; 17 17 17];
    D(:,:,2,18) = [ -9 -9 -9; -9  0 -9; 17 17 17];
    D(:,:,3,18) = [ -9 -9 -9; -9 -9 -9; -9 -9 -9];
    lgth = 18;
  otherwise
    error('Do not recognize mask ''%s''. Only use: kirsch, 3d, 3d-kirsch, or 3d-sobel',masks);
end

if isempty(data)
  %% Extract images info
  strNames = dir(faceDir);
  str = struct2cell(strNames(3:end));
  fullNames = strcat(repmat(faceDir,length(str),1),str(1,:)');
  numFaces = length(fullNames);

else
  % in case of given data use the info from the data instead
  numFaces = size(data,3);
end
    
  nNumFaces = length(numFaces);

%% Process the code
% extract the faces & process the ldp code
switch lower(masks)
  case 'kirsch'
    k = 1; % use a different index as the validFaces may not be consecutive
    for i = numFaces
      if isempty(data)
        I = imresize(imread(fullNames{i}), [r c]);
      else
        I = imresize(data(:,:,i), [r c]);
      end      
      if(size(I,3) ~= 1)
        I = rgb2gray(I);
      end
      I = double(I);
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % LDP part
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      R = zeros(r,c,lgth);
      for j=1:lgth
        R(:,:,j) = imfilter(I,D(:,:,j));
      end
      % sort responses, from most negative to most possitive
      [~, idx] = sort((R),3,'descend');
      % return only the indexes, equivalent to orientation direction
      % encoding [edge gradient]
      top = idx(:,:,1:topRank);
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      sCodes(:,:,k,:) = top; % spatial codes
      k = k + 1;
    end
  case {'3d', '3d-sobel', '3d-kirsch'}
    % create the 3D volume
    k = 1;
    imgs = zeros(r,c,nNumFaces);
    for i = numFaces
      if isempty(data)
        I = imresize(imread(fullNames{i}), [r c]);
      else
        I = imresize(data(:,:,i), [r c]);
      end
      
      if(size(I,3) ~= 1)
        I = rgb2gray(I);
      end
      imgs(:,:,k) = double(I);
      k = k+1;
    end
    R = zeros([size(imgs) lgth]);
    for j=1:lgth
      M(:,:,:) = D(:,:,:,j);
      R(:,:,:,j) = convn(imgs,M(end:-1:1,end:-1:1,end:-1:1),'same');
    end
    % sort responses, from most negative to most possitive
    [~, idx] = sort(abs(R),4,'descend');
    sCodes = idx(:,:,:,1:topRank);
end

%% Create the histograms (matrices of change)

codeLength = lgth^2;
t = nNumFaces; % just for quick coding
rp = floor(r/grid(1)); % row step
cp = floor(c/grid(2)); % column step
tp = floor(t/grid(3)); % time step
if(tp == 0), tp = 1; end; % step 1 if the grid is bigger than the size of available frames

if(overlapping > 0)
  over = 1 - overlapping;
  if over == 0,over = 0.001; end; % 1% overlap if zero
  orp = round(rp*over); if orp == 0, orp = 1; end; % overlap row step
  ocp = round(cp*over); if ocp == 0, ocp = 1; end; % overlap col step
  otp = round(tp*over); if otp == 0, otp = 1; end; % overlap time step
  
  nr = floor((grid(1)-1+over)/over);
  nc = floor((grid(2)-1+over)/over);
  nt = floor((grid(3)-1+over)/over);
else
  orp = rp;
  ocp = cp;
  otp = tp;
  
  nr = grid(1);
  nc = grid(2);
  nt = grid(3);
end

% compute init positions for blocks (avoid adding the size as possible
% possition and skip smaller blocks at the end)
riv = 1:orp:r; % row initial vector
riv = riv(1:nr);
civ = 1:ocp:c; % col initial vector
civ = civ(1:nc);
tiv = 1:otp:t; % time initial vector
if (length(tiv) < nt), tiv = [tiv repmat(t,1,nt-length(tiv))]; end
tiv = tiv(1:nt);

H = zeros(1,codeLength*length(riv)*length(civ)*length(tiv)*topRank);
for trk=1:topRank
  % generate the codes
  C(:,:,:) = sCodes(:,:,:,trk);
  
  for i=1:length(riv)
    for j=1:length(civ)
      for k=1:length(tiv)
        % find the indexes to extract the blocks
        ri = riv(i);
        rf = ri + rp - 1;
        if (rf > r) || (i == length(riv) && rf < r) || (rp ==0), rf = r; end;
        
        ci = civ(j);
        cf = ci + cp - 1;
        if (cf > c) || (j == length(civ) && cf < c) || (cp ==0), cf = c; end;
        
        ti = tiv(k);
        tf = ti + tp - 1;
        if (tf > t) || (k == length(tiv) && tf < t) || (tp ==0), tf = t; end;
        % extract the matrix
        mat = zeros(lgth);
        if (ti ~= tf)
          strt = C(ri:rf, ci:cf, ti:tf-1);
          lst = C(ri:rf, ci:cf, ti+1:tf); % shift the next frame to compute the changes
        else
          strt = C(ri:rf, ci:cf, ti);
          lst = strt;
        end
        idx = sub2ind([lgth lgth],strt(:),lst(:));
        uidx = unique(idx(:));
        cnt = hist(idx,numel(uidx));
        mat(uidx) = cnt;
        % normilized
        if normilized
          s = sum(mat(:));
          s(s==0) = 1;
          mat = mat./s;
        end
        % compute the index of the histogram
        el = (trk-1)*prod(grid) + (k-1)*grid(1)*grid(2) + (i-1)*grid(2) + j;
        hi = (el-1)*codeLength + 1;
        hf = el*codeLength;
        H(hi:hf) = mat(:);
      end
    end
  end
end